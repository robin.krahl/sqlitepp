VERSION_MAJOR := 0
VERSION_MINOR := 1
VERSION_PATCH := 0

LDFLAGS_SQLITE3 += -lsqlite3
LDFLAGS_GTEST += -lgtest -lgtest_main

CPPFLAGS +=
CXXFLAGS += -std=c++11 -Wall -Wextra
