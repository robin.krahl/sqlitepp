include config.mk

VERSION := $(VERSION_MAJOR).$(VERSION_MINOR).$(VERSION_PATCH)

INCLUDE_DIR := include
SOURCE_DIR := src
OBJECT_DIR := obj
LIBRARY_DIR := lib
BINARY_DIR := bin
DOCUMENTATION_DIR := doc

SOURCE_NAMES_LIB := sqlitepp.cpp
SOURCE_NAMES_TEST := sqlitepp_test.cpp

TARGET_NAME_LIB := sqlitepp
TARGET_LIB_BASE := $(LIBRARY_DIR)/lib$(TARGET_NAME_LIB).so
TARGET_LIB_LINKS := $(TARGET_LIB_BASE) $(TARGET_LIB_BASE).$(VERSION_MAJOR)
TARGET_LIB := $(TARGET_LIB_BASE).$(VERSION)
SOURCES_LIB := $(foreach source,$(SOURCE_NAMES_LIB),$(SOURCE_DIR)/$(source))
OBJECTS_LIB := $(SOURCES_LIB:$(SOURCE_DIR)/%.cpp=$(OBJECT_DIR)/%.o)
DEPENDS_LIB := $(OBJECTS_LIB:.o=.d)

TARGET_TEST := $(BINARY_DIR)/sqlitepp_test
SOURCES_TEST := $(foreach source,$(SOURCE_NAMES_TEST),$(SOURCE_DIR)/$(source))
OBJECTS_TEST := $(SOURCES_TEST:$(SOURCE_DIR)/%.cpp=$(OBJECT_DIR)/%.o)
DEPENDS_TEST := $(OBJECTS_TEST:.o=.d)

TARGETS := $(TARGET_LIB) $(TARGET_LIB_LINKS) $(TARGET_TEST)
INCLUDES := $(INCLUDE_DIR)/sqlitepp/sqlitepp.h
OBJECTS := $(OBJECTS_LIB) $(OBJECTS_TEST)
DEPENDS := $(DEPENDS_LIB) $(DEPENDS_TEST)

CPPFLAGS += -MMD -MP -I$(INCLUDE_DIR)
CPPFLAGS += -DSQLITEPP_VERSION_MAJOR=$(VERSION_MAJOR) \
	    -DSQLITEPP_VERSION_MINOR=$(VERSION_MINOR) \
	    -DSQLITEPP_VERSION_PATCH=$(VERSION_PATCH) \
	    -DSQLITEPP_VERSION=\"$(VERSION)\"
CXXFLAGS += -fPIC
LDFLAGS += $(LDFLAGS_SQLITE3)
LDFLAGS_LIB += -Wl,-soname,lib$(TARGET_NAME_LIB).so.$(VERSION_MAJOR)
LDFLAGS_TEST += -L$(LIBRARY_DIR) -l$(TARGET_NAME_LIB) $(LDFLAGS_GTEST) -Wl,-rpath,$(LIBRARY_DIR)

CLANG_TIDY_CHECKS := "clang*,cppcoreguidelines*,modernize*,readability*"

ifdef VERBOSE
	QUIET :=
else
	QUIET := @
endif

.PHONY = all checkstyle clean doc memcheck test

all: $(TARGETS)

clean:
	$(info (RM)    $(OBJECT_DIR))
	$(QUIET)$(RM) -r $(OBJECT_DIR)
	$(info (RM)    $(LIBRARY_DIR))
	$(QUIET)$(RM) -r $(LIBRARY_DIR)
	$(info (RM)    $(BINARY_DIR))
	$(QUIET)$(RM) -r $(BINARY_DIR)
	$(info (RM)    $(DOCUMENTATION_DIR))
	$(QUIET)$(RM) -r $(DOCUMENTATION_DIR)

checkstyle:
	clang-tidy --checks=$(CLANG_TIDY_CHECKS) \
		$(INCLUDES) $(SOURCES_LIB) \
		-- -x c++ $(CPPFLAGS) $(CXXFLAGS)

doc: $(INCLUDES)
	(cat Doxyfile ; echo "PROJECT_NUMBER=$(VERSION)" ) | doxygen -

memcheck: $(TARGET_TEST)
	valgrind --tool=memcheck ./$(TARGET_TEST)

test: $(TARGET_TEST)
	./$(TARGET_TEST)

$(TARGET_LIB_LINKS): $(TARGET_LIB)
	$(info (LN)    $@)
	$(QUIET)ln -sr $^ $@

$(TARGET_LIB): $(OBJECTS_LIB)
	$(info (CXX)   $@)
	$(QUIET)mkdir -p $(LIBRARY_DIR)
	$(QUIET)$(CXX) -shared $(LDFLAGS) $(LDFLAGS_LIB) -o $@ $^

$(TARGET_TEST): $(OBJECTS_TEST) $(TARGET_LIB) $(TARGET_LIB_BASE)
	$(info (CXX)   $@)
	$(QUIET)mkdir -p $(BINARY_DIR)
	$(QUIET)$(CXX) $(LDFLAGS) $(LDFLAGS_TEST) $(OBJECTS_TEST) -o $@

$(OBJECTS): $(OBJECT_DIR)/%.o: $(SOURCE_DIR)/%.cpp
	$(info (CXX)   $@)
	$(QUIET)mkdir -p $(OBJECT_DIR)
	$(QUIET)$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

-include $(DEPENDS)
